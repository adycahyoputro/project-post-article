package com.tes.postarticle.service;

import java.util.List;

import org.hibernate.type.TextType;
import org.hibernate.type.descriptor.sql.LongVarcharTypeDescriptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tes.postarticle.models.postModel;
import com.tes.postarticle.repository.postRepo;

@Service
@Transactional
public class postService {
	
	@Autowired
	private postRepo postrepo;
	
	public List<postModel> listPost(){
		return postrepo.listPost();
	}
	
	public List<postModel> listPostPublish(){
		return postrepo.listPostPublish();
	}
	
	public List<postModel> listPostDraft(){
		return postrepo.listPostDraft();
	}
	
	public List<postModel> listPostTrash(){
		return postrepo.listPostTrash();
	}
	
	public postModel listPostById(int id) {
		return postrepo.listPostById(id);
	}
	
	public void insertPost(String title, String content, String category, String status) {
		postrepo.insertPost(title, content, category, status);
	}
	
	public void updatePost(String title, String content, String category, String status, int id) {
		postrepo.updatePost(title, content, category, status, id);
	}
	
	public void deletePost(int id) {
		postrepo.deletePost(id);
	}
	
	public void deletePostById(int id) {
		postrepo.deletePostById(id);
	}
}
