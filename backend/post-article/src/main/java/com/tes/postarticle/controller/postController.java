package com.tes.postarticle.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tes.postarticle.models.postModel;
import com.tes.postarticle.service.postService;

@CrossOrigin(origins = "http://localhost:8080", maxAge = 3600)
@RestController
@RequestMapping(value = "/api")
public class postController {
	
	@Autowired
	private postService postservice;
	
	@GetMapping(value = "/article")
	public List<postModel> listPost(){
		return postservice.listPost();
	}
	
	@GetMapping(value = "/article/publish")
	public List<postModel> listPostPublish(){
		return postservice.listPostPublish();
	}
	
	@GetMapping(value = "/article/draft")
	public List<postModel> listPostDraft(){
		return postservice.listPostDraft();
	}
	
	@GetMapping(value = "/article/trash")
	public List<postModel> listPostTrash(){
		return postservice.listPostTrash();
	}
	
	@GetMapping(value = "/article/{id}")
	public postModel listPostById(@PathVariable int id) {
		return postservice.listPostById(id);
	}
	
	@Modifying
	@PostMapping(value = "/article")
	public void insertPost(@RequestBody postModel postmodel) {
		postservice.insertPost(postmodel.getTitle(), postmodel.getContent(), postmodel.getCategory(), postmodel.getStatus());
	}
	
	@Modifying
	@PutMapping(value = "/article")
	public void updatePost(@RequestBody postModel postmodel) {
		postservice.updatePost(postmodel.getTitle(), postmodel.getContent(), postmodel.getCategory(), postmodel.getStatus(), postmodel.getId());
	}
	
	@Modifying
	@PutMapping(value = "/article/{id}")
	public void deletePost(@PathVariable int id) {
		postservice.deletePost(id);
	}
	
	@Modifying
	@DeleteMapping(value = "/article/{id}")
	public void deletePostById(@PathVariable int id) {
		postservice.deletePostById(id);
	}

}
