package com.tes.postarticle.repository;

import java.util.List;

import org.hibernate.type.descriptor.sql.LongVarcharTypeDescriptor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.tes.postarticle.models.postModel;

@Repository
public interface postRepo extends JpaRepository<postModel, Integer>{

	@Query(value = "select * from posts order by id", nativeQuery = true)
	List<postModel> listPost();
	
	@Query(value = "select * from posts where status='publish' order by id", nativeQuery = true)
	List<postModel> listPostPublish();
	
	@Query(value = "select * from posts where status='draft' order by id", nativeQuery = true)
	List<postModel> listPostDraft();
	
	@Query(value = "select * from posts where status='trash' order by id", nativeQuery = true)
	List<postModel> listPostTrash();
	
	@Query(value = "select * from posts where id=?1", nativeQuery = true)
	postModel listPostById(int id);
	
	@Modifying
	@Query(value = "insert into posts(title,content,category,created_date,updated_date,status)"
			+ "values(?1,?2,?3,now(),NULL,?4)", nativeQuery = true)
	void insertPost(String title, LongVarcharTypeDescriptor content, String category, String status);
	
	@Modifying
	@Query(value = "update posts set title=?1, content=?2, category=?3, updated_date=now(), status=?4 where id=?5", nativeQuery = true)
	void updatePost(String title, LongVarcharTypeDescriptor content, String category, String status, int id);
	
	@Modifying
	@Query(value = "update posts set status='trash' where id=?1", nativeQuery = true)
	void deletePost(int id);
	
	@Modifying
	@Query(value = "delete from posts where id=?1", nativeQuery = true)
	void deletePostById(int id);
	
}
