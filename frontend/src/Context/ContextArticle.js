import axios, { Axios } from "axios";
import React, { createContext, useState } from "react";
import { useHistory } from "react-router-dom";

export const ArticleContext = createContext();

export const ArticleProvider = props => {

    let history = useHistory();
    const [listArticlePublish, setListArticlePublish] = useState([]);
    const [listArticleDraft, setListArticleDraft] = useState([]);
    const [listArticleTrash, setListArticleTrash] = useState([]);
    const [inputName, setInputName] = useState({
        title : "",
        content : "",
        category : "",
        status : ""
    })
    const [currentId, setCurrentId] = useState(null);
    const [fetchStatus, setFetchStatus] = useState(true);
    const [select, setSelect] = useState();
    const [input, setInput] = useState([]);

    let stateArticle = {
        listArticlePublish, setListArticlePublish, listArticleDraft, setListArticleDraft, listArticleTrash, setListArticleTrash, inputName, setInputName, currentId, setCurrentId, fetchStatus, setFetchStatus,select, setSelect
    }

    // let stateArticleDraft = {
    //     listArticlePublish, setListArticlePublish, listArticleDraft, setListArticleDraft, listArticleTrash, setListArticleTrash, inputName, setInputName, currentId, setCurrentId, fetchStatus, setFetchStatus,select, setSelect
    // }

    // let stateArticleTrash = {
    //     listArticlePublish, setListArticlePublish, listArticleDraft, setListArticleDraft, listArticleTrash, setListArticleTrash, inputName, setInputName, currentId, setCurrentId, fetchStatus, setFetchStatus,select, setSelect
    // }

    const fetchDataPublish = async () => {

        try {
            const {data} = await axios.get('https://backend-postarticle.herokuapp.com/api/article/publish');
            let resultPublish = data.map((res) => {
                let {
                    id,
                    title,
                    content,
                    category,
                    created_date,
                    updated_date,
                    status
                } = res
                return {
                    id,
                    title,
                    content,
                    category,
                    created_date,
                    updated_date,
                    status
                }
            })
            setListArticlePublish([...resultPublish]);
        }catch (error){
            console.info(error);
            alert("data gagal ditampilkan");
        }
    }

    const fetchDataDraft = async () => {

        try {
            const {data} = await axios.get(`https://backend-postarticle.herokuapp.com/api/article/draft`);
            let resultDraft = data.map((res) => {
                let {
                    id,
                    title,
                    content,
                    category,
                    created_date,
                    updated_date,
                    status
                } = res
                return {
                    id,
                    title,
                    content,
                    category,
                    created_date,
                    updated_date,
                    status
                }
            })
            setListArticleDraft([...resultDraft]);
        }catch (error){
            console.log(error);
            alert("data gagal ditampilkan");
        }
    }

    const fetchDataTrash = async () => {

        try {
            const {data} = await axios.get(`https://backend-postarticle.herokuapp.com/api/article/trash`);
            let result = data.map((res) => {
                let {
                    id,
                    title,
                    content,
                    category,
                    created_date,
                    updated_date,
                    status
                } = res
                return {
                    id,
                    title,
                    content,
                    category,
                    created_date,
                    updated_date,
                    status
                }
            })
            setListArticleTrash([...result]);
        }catch (error){
            alert("data gagal ditampilkan");
        }
    }

    const handleChange = (event) => {
        // let {name, value} = event.target;
        let value = event.target.value;
        let name = event.target.name;
        // let status = setSelect(event.target.value);
        setInputName({...inputName, [name] : value});
        // handleChange();
    }

    const handleSubmit = (event) => {
        event.preventDefault();

        if(currentId === null){
            axios.post(`https://backend-postarticle.herokuapp.com/api/article`, {
                title : inputName.title,
                content : inputName.content,
                category : inputName.category,
                status : inputName.status
            })
            .then(() => {
                setFetchStatus(true);
                history.push('/');
            })
            .catch((error) => {
                console.log(error);
            })
        }else {
            axios.put(`https://backend-postarticle.herokuapp.com/api/article`, {
                id : inputName.id,
                title : inputName.title,
                content : inputName.content,
                category : inputName.category,
                status : inputName.status
            })
            .then(() => {
                setFetchStatus(true);
                history.push('/');
            })
            .catch((error) => {
                console.log(error);
            })
        }
        setInputName({
            title: "",
            content: "",
            category: "",
            status:""
        });
        setCurrentId(null);
    }

    const handleEdit = (event) => {
        let idArticle = parseInt(event.currentTarget.value);
        axios.get(`https://backend-postarticle.herokuapp.com/api/article/${idArticle}`)
        .then(res => {
            let data = res.data;
            setCurrentId(idArticle);
            setInputName({
                id: data.id,
                title : data.title,
                content : data.content,
                category : data.category,
                status : data.status
            });
            history.push(`/article/edit/${idArticle}`);
        })
    }

    const handleDeleteTrash = (event) => {
        let idArticle = parseInt(event.currentTarget.value);
        if(window.confirm("hapus data ini?")){
            axios.put(`https://backend-postarticle.herokuapp.com/api/article/${idArticle}`)
            .then(() => {
                setFetchStatus(true);
            })
            .catch((error) => {
                console.log(error);
            })
        }
    }

    const handleDelete = (event) => {
        let idArticle = parseInt(event.currentTarget.value);
        if(window.confirm("hapus data ini?")){
            axios.delete(`https://backend-postarticle.herokuapp.com/api/article/${idArticle}`)
            .then(() => {
                setFetchStatus(true);
            })
            .catch((error) => {
                console.log(error);
            })
        }
    }

    const handleText = (text, max) => {
        if (text === null) {
            return "";
        } else {
            return text.substring(0,max) + "...";
        }
    }

    const handleDate = (value) => {
        let date = new Date(value);
        return date.getDate()+"/"+(date.getMonth()+1)+"/"+date.getFullYear()+" "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds();
    }

    let handleFunction = {
        fetchDataPublish,
        fetchDataDraft,
        fetchDataTrash,
        handleChange,
        handleSubmit,
        handleEdit,
        handleDeleteTrash,
        handleDelete,
        handleText,
        handleDate
    }

    // let handleFunctionDraft = {
    //     fetchDataDraft,
    //     handleChange,
    //     handleSubmit,
    //     handleEdit,
    //     handleDeleteTrash,
    //     handleDelete
    // }

    // let handleFunctionTrash = {
    //     fetchDataTrash,
    //     handleChange,
    //     handleSubmit,
    //     handleEdit,
    //     handleDeleteTrash,
    //     handleDelete
    // }

    return (
        <ArticleContext.Provider value={{stateArticle, handleFunction}}>
            {props.children}
        </ArticleContext.Provider>
    )
}