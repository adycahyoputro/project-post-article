import React, { useContext } from "react";
import 'antd/dist/antd.css';
import '../Assets/Css/Style.css';
import { Card } from "antd";
import { ArticleContext } from "../Context/ContextArticle";

const FormArticle = () => {

    const {stateArticle, handleFunction} = useContext(ArticleContext);

    let {
        listArticlePublish, setListArticlePublish, listArticleDraft, setListArticleDraft, listArticleTrash, setListArticleTrash, inputName, setInputName, currentId, setCurrentId, fetchStatus, setFetchStatus, select, setSelect
    } = stateArticle;

    let {handleChange, handleSubmit} = handleFunction;

    return (
        <>
            <div className="formArticle">
                <Card title="ARTICLE FORM" style={{ width: 500, border : '2px solid black'}} className="cardBody">
                    <form onSubmit={handleSubmit} method="POST">
                        <input onChange={handleChange} type="text" id="id" value={inputName.id} hidden/>
                        <label htmlFor="title">Title</label>
                        <input onChange={handleChange} type="text" id="title" name="title" value={inputName.title} placeholder="Your title.." required/>

                        <label htmlFor="content">Content</label>
                        <textarea onChange={handleChange} id="content" name="content" value={inputName.content} placeholder="Your content.." required/>

                        <label htmlFor="category">Category</label>
                        <input onChange={handleChange} type="text" id="category" name="category" value={inputName.category} placeholder="Your category.." required/>

                        <label htmlFor="status">Status</label>
                        <select name="status" onChange={handleChange} defaultValue={inputName.status} required>
                            <option value="">Pilih</option>
                            <option value="publish">Publish</option>
                            <option value="draft">Draft</option>
                        </select>
                    
                        <input type="submit" value="Submit" />
                    </form>
                </Card>
            </div>
        </>
    )
}

export default FormArticle;