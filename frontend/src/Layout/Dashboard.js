import React, { useState } from "react";
import { Layout, Menu } from 'antd';
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  UserOutlined,
  VideoCameraOutlined,
  UploadOutlined,
} from '@ant-design/icons';
import { Link } from "react-router-dom";
import 'antd/dist/antd.css';
import '../Assets/Css/Style.css';

const { Header, Sider, Content } = Layout;

const Dashboard = (props) => {

    const [state, setState] = useState({
        collapsed: false,
      });
    
    const toggle = () => {
        setState({
          collapsed: !state.collapsed,
        });
      };

    return (
        <>
            {props.main && (
            <Layout>
                <Sider trigger={null} collapsible collapsed={state.collapsed}>
                <div className="logo"><h1>ARTICLE</h1></div>
                <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
                    <Menu.Item key="1" icon={<UserOutlined />}>
                        <Link to={'/'}>All Post</Link>
                    </Menu.Item>
                    <Menu.Item key="2" icon={<VideoCameraOutlined />}>
                        <Link to={'/article/insert'}>Add New</Link>
                    </Menu.Item>
                    <Menu.Item key="3" icon={<UploadOutlined />}>
                        <Link to={'/article/preview'}>Preview</Link>
                    </Menu.Item>
                </Menu>
                </Sider>
                <Layout className="site-layout">
                <Header className="site-layout-background" style={{ padding: 0 }}>
                    {React.createElement(state.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
                    className: 'trigger',
                    onClick: toggle,
                    })}
                </Header>
                <Content
                    className="site-layout-background"
                    style={{
                    margin: '24px 16px',
                    padding: 24,
                    minHeight: 280,
                    }}
                >
                {props.content}
                </Content>
                </Layout>
            </Layout>
            )}
        </>
    )
}

export default Dashboard;