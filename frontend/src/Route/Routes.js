import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    Redirect
} from 'react-router-dom';
import Dashboard from "../Layout/Dashboard";
import AllPost from "../Pages/AllPost";
import '../Assets/Css/Style.css';
import { ArticleProvider } from "../Context/ContextArticle";
import FormArticle from "../Layout/FormArticle";
import Preview from "../Pages/Preview";

const Routes = () => {

    return (
        <>
            <Router>
                <ArticleProvider>
                <Switch>
                    <Route path="/" exact>
                        <Dashboard main content={<AllPost/>}/>
                    </Route>
                    <Route path="/article/insert" exact>
                        <Dashboard main content={<FormArticle/>}/>
                    </Route>
                    <Route path="/article/edit/:slug" exact>
                        <Dashboard main content={<FormArticle/>}/>
                    </Route>
                    <Route path="/article/preview" exact>
                        <Dashboard main content={<Preview/>}/>
                    </Route>
                </Switch>
                </ArticleProvider>
            </Router>
        </>
    )
}

export default Routes;