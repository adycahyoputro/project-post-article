import { Card, Divider, Pagination } from "antd";
import React, { useContext, useEffect } from "react";
import index from "react-highlight-words";
import { ArticleContext } from "../Context/ContextArticle";

const Preview = () => {

    const {stateArticle, handleFunction} = useContext(ArticleContext);

    let {
        listArticlePublish, setListArticlePublish, listArticleDraft, setListArticleDraft, listArticleTrash, setListArticleTrash, inputName, setInputName, currentId, setCurrentId, fetchStatus, setFetchStatus
    } = stateArticle;

    let {fetchDataPublish, handleEdit, handleDeleteTrash, handleText, handleDate} = handleFunction;

    useEffect(() => {
        if(fetchStatus){
            fetchDataPublish();
            setFetchStatus(false);
        }
    }, [fetchStatus, setFetchStatus]);

    return (
        <>
            <div className="container">
                {
                    listArticlePublish !== null &&
                    (
                        <>
                            {
                                listArticlePublish.map((val, index) => {
                                    return (
                                        <>
                                            {
                                                <Card title={val.title} extra={<p>created at {handleDate(val.created_date)}</p>} style={{width: '100%'}}>
                                                    <p style={{textAlign: 'justify'}}>{val.content}</p>
                                                    <p>Kategori : {val.category}</p>
                                                    <p>Status : {val.status}</p>
                                                </Card>
                                            }
                                            <Divider />
                                        </>
                                    )
                                })
                            }
                            {/* <Pagination defaultCurrent={1} total={2} /> */}
                        </>
                    )
                }

            </div>
        </>
    )
}

export default Preview;