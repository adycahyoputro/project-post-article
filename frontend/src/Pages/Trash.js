import React, { useContext, useEffect } from "react";
import { ArticleContext } from "../Context/ContextArticle";
import { Table, Input, Button, Space } from 'antd';
import { DeleteOutlined, EditOutlined } from "@ant-design/icons";

const Trash = () => {

    const {stateArticle, handleFunction} = useContext(ArticleContext);

    let {
        listArticlePublish, setListArticlePublish, listArticleDraft, setListArticleDraft, listArticleTrash, setListArticleTrash, inputName, setInputName, currentId, setCurrentId, fetchStatus, setFetchStatus
    } = stateArticle;

    let {fetchDataTrash, handleEdit, handleDelete, handleText} = handleFunction;

    useEffect(() => {
        if(fetchStatus){
            fetchDataTrash();
            setFetchStatus(false);
        }
    }, [fetchStatus, setFetchStatus]);

    const data = listArticleTrash;

    const columns = [
        {
          title: 'Title',
          dataIndex: 'title',
          render: title => <p>{handleText(title, 50)}</p>,
          key: 'title',
        },
        {
          title: 'Content',
          dataIndex: 'content',
          render: content => <p>{handleText(content, 100)}</p>,
          key: 'content',
          sorter: {
            compare: (a, b) => a.content - b.content,
            multiple: 3,
          },
        },
        {
          title: 'Category',
          dataIndex: 'category',
          sorter: {
            compare: (a, b) => a.category - b.category,
            multiple: 2,
          },
        },
        {
          title: 'Status',
          dataIndex: 'status',
          sorter: {
            compare: (a, b) => a.status - b.status,
            multiple: 1,
          },
        },
        {
          title: 'Action',
          key: 'action',
          render: (res, index) => (
              <div>
                  <li key={index}></li>
                  {/* <button onClick={handleEdit} value={res.id}><EditOutlined /></button> */}
                  <button onClick={handleDelete} value={res.id}><DeleteOutlined /></button>
              </div>
          ),
        },
      ];

      function onChange(pagination, filters, sorter, extra) {
        console.log('params', pagination, filters, sorter, extra);
      }

    return (
        <>
            <Table columns={columns} dataSource={data} onChange={onChange} />
        </>
    )
    
}

export default Trash;