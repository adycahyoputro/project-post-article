import React, { useContext, useEffect } from "react";
import { ArticleContext, ArticleProvider } from "../Context/ContextArticle";
import { Table, Input, Button, Space, Tabs } from 'antd';
import Publish from "./Publish";
import Draft from "./Draft";
import Trash from "./Trash";
import 'antd/dist/antd.css';

const AllPost = () => {

    const {stateArticle, handleFunction} = useContext(ArticleContext);

    let {
        listArticlePublish, setListArticlePublish, listArticleDraft, setListArticleDraft, listArticleTrash, setListArticleTrash, inputName, setInputName, currentId, setCurrentId, fetchStatus, setFetchStatus
    } = stateArticle;

    let {fetchDataPublish, fetchDataDraft, fetchDataTrash} = handleFunction;

    const { TabPane } = Tabs;

    return (
        <>
            <Tabs centered>
                <TabPane tab="Published" key="1">
                    <Publish />
                </TabPane>
                <TabPane tab="Drafts" key="2">
                    <Draft />
                </TabPane>
                <TabPane tab="Trashed" key="3">
                    <Trash />
                </TabPane>
            </Tabs>
            <div style={{display: 'none'}}>
                <Publish />
                <Draft />
                <Trash />
            </div>
        </>
    )
    
}

export default AllPost;