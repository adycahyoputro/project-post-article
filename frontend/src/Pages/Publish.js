import React, { useContext, useEffect } from "react";
import { ArticleContext } from "../Context/ContextArticle";
import { Table, Input, Button, Space } from 'antd';
import { DeleteOutlined, EditOutlined } from "@ant-design/icons";

const Publish = () => {

    const {stateArticle, handleFunction} = useContext(ArticleContext);

    let {
        listArticlePublish, setListArticlePublish, listArticleDraft, setListArticleDraft, listArticleTrash, setListArticleTrash, inputName, setInputName, currentId, setCurrentId, fetchStatus, setFetchStatus
    } = stateArticle;

    let {fetchDataPublish, handleEdit, handleDeleteTrash, handleText} = handleFunction;

    useEffect(() => {
        if(fetchStatus){
            fetchDataPublish();
            setFetchStatus(false);
        }
    }, [fetchStatus, setFetchStatus]);

    // const data = listArticlePublish;
    const data = listArticlePublish;

    const columns = [
        {
          title: 'Title',
          dataIndex: 'title',
          render: title => <p>{handleText(title, 50)}</p>,
          key: 'title',
        },
        {
          title: 'Content',
          dataIndex: 'content',
          render: content => <p>{handleText(content, 100)}</p>,
          key: 'content',
          sorter: {
            compare: (a, b) => a.content.length - b.content.length,
            multiple: 3,
          },
        },
        {
          title: 'Category',
          dataIndex: 'category',
          sorter: {
            compare: (a, b) => a.category.length - b.category.length,
            multiple: 2,
          },
        },
        {
          title: 'Status',
          dataIndex: 'status',
          sorter: {
            compare: (a, b) => a.status.length - b.status.length,
            multiple: 1,
          },
        },
        {
            title: 'Action',
            key: 'action',
            render: (res, index) => (
                <div>
                    <li key={index}></li>
                    <button onClick={handleEdit} value={res.id}><EditOutlined /></button>
                    <button onClick={handleDeleteTrash} value={res.id}><DeleteOutlined /></button>
                </div>
            ),
          },
      ];

      function onChange(pagination, filters, sorter, extra) {
        console.log('params', pagination, filters, sorter, extra);
      }

    return (
        <>
            <Table columns={columns} dataSource={data} onChange={onChange}/>
        </>
    )
    
}

export default Publish;